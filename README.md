GitLab is a web-based DevOps lifecycle tool that provides a Git-repository 
manager providing wiki, issue-tracking and CI/CD pipeline features, 
using an open-source license, developed by GitLab Inc. 